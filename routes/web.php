<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//Route::get('/', 'HomeController@index')->name('index');
Route::get('/', 'HomeController@dashboard')->name('dashboard');
Route::post('/add', 'HomeController@add')->name('add');
Route::get('/delete/{id}', 'HomeController@delete')->name('delete');
Route::get('/create', 'HomeController@create')->name('create');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/export', 'HomeController@export')->name('export');
