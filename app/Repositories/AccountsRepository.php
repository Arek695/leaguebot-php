<?php


namespace App\Repositories;


use App\Interfaces\AccountsInterface;
use Illuminate\Support\Facades\Auth;
use RiotAPI\LeagueAPI\LeagueAPI;
use RiotAPI\LeagueAPI\Definitions\Region;
use App\Account;
class AccountsRepository implements AccountsInterface
{
    private $api;


    public static function getSummonerLvl($name, $region)
    {
        $api = new LeagueAPI([
            //  Your API key, you can get one at https://developer.riotgames.com/
            LeagueAPI::SET_KEY    => 'RGAPI-b5274690-c0ba-4110-973a-d5ac0cc96673',
            //  Target region (you can change it during lifetime of the library instance)
                LeagueAPI::SET_REGION => $region,
        ]);

        return $api->getSummonerByName($name)->summonerLevel;
    }

    public static function getAccountsCount()
    {
        return Account::where('user_id', Auth::user()->id)->count();
    }

    public static function getAccounts()
    {
        return Account::where('user_id', Auth::user()->id)->get();
    }
}
