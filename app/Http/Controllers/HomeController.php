<?php

namespace App\Http\Controllers;

use App\Account;
use App\Interfaces\AccountsInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use File;
class HomeController extends Controller
{

    public function __construct(AccountsInterface $aR)
    {
        $this->aR = $aR;
        $this->middleware('auth');
    }


    public function dashboard()
    {
        $accounts = $this->aR->getAccounts();
        $count = $this->aR->getAccountsCount();
        return view('dashboard', ['summoner' => $accounts, 'count' => $count]);
    }

    public function create()
    {
        return view('create');
    }

    public function delete($id)
    {
        Account::find($id)->delete();
        return Redirect()->back();

    }

    public function add(Request $request)
    {
        $accounts = explode("\n", str_replace("\r", "", $request->accounts));
        $cyk = explode(":",$accounts[0]);
        foreach($accounts as $acc)
        {
            Account::insert([
                'name' => $cyk[1],
                'region' => strtolower($cyk[0]),
                'password' => $cyk[2],
                'user_id' => Auth::user()->id,
            ]);
        }
        return Redirect()->back();
    }

    public function profile()
    {
        return view("profile");
    }

    public function export()
    {
        $accounts = Account::where('user_id', Auth::user()->id)->select('region','name','password')->get();
        $data = $accounts;
        $destinationPath=public_path()."/upload/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath."accounts.txt",$data);
        return response()->download($destinationPath."accounts.txt");
    }
}
