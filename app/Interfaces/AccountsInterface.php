<?php


namespace App\Interfaces;


interface AccountsInterface
{

    public static function getSummonerLvl($name, $region);
    public static function getAccounts();
    public static function getAccountsCount();

}
