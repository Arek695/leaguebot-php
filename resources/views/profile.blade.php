@extends('layouts.app')
@section('content')
<!-- Page Header-->
<div class="page-header no-margin-bottom">
    <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit profile</h2>
    </div>
</div>
<!-- Breadcrumb-->
<div class="container-fluid">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active">Edit profile</li>
    </ul>
</div>
<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="block">
                    <div class="title"><strong>Edit profile</strong></div>
                    <div class="block-body">
                        <form class="form-horizontal">
                            <div class="line"> </div>
                            <div class="row">
                                <label class="col-sm-3 form-control-label">
                                    <div class="avatar"><img src="img/avatar-6.jpg" alt="..." class="img-fluid rounded-circle"></div>
                                    <a class="btn btn-link"  href="#">
                                        Change avatar
                                    </a>
                                </label>
                                <div class="col-sm-9">
                                    <div class="form-group-material">
                                        <input id="register-username" type="text" name="registerUsername" required class="input-material">
                                        <label for="register-username" class="label-material">{{Auth::user()->name}}</label>
                                    </div>
                                    <div class="form-group-material">
                                        <input id="register-email" type="email" name="registerEmail" required class="input-material">
                                        <label for="register-email" class="label-material">{{Auth::user()->email}}      </label>
                                    </div>
                                    <div class="form-group-material">
                                        <input id="register-password" type="password" name="registerPassword" required class="input-material">
                                        <label for="register-password" class="label-material">Password        </label>
                                    </div>
                                    <div class="form-group-material">
                                        <input id="repeat-register-password" type="password" name="repeatregisterPassword" required class="input-material">
                                        <label for="repeat-register-password" class="label-material">Repeat password        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <div class="col-sm-9 ml-auto">
                                    <button type="submit" class="btn btn-secondary">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
            <!-- Please do not remove the backlink to us unless you support us at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            <p class="no-margin-bottom">2019 &copy; Your company. Design by <a href="https://bootstrapious.com/p/bootstrap-4-dark-admin">Bootstrapious</a>.</p>
        </div>
    </div>
</footer>

<!-- Modal-->
<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><strong id="exampleModalLabel" class="modal-title">Edit bot</strong>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Accounts</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
    <!--ENDMODAL-->
@endsection
