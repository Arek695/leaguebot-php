@extends('layouts.app')
@section('content')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- Page Header-->
<div class="page-header no-margin-bottom">
    <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">ABC League bot</h2>
    </div>
</div>
<!-- Breadcrumb-->
<div class="container-fluid">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item active">Your dashboard</li>
        <div style="margin-left: 73%">
        <a href="/export" type="submit" class="btn btn-primary text-white float-left ">Export accounts</a>
        <button  data-toggle="modal" data-target="#myModal" type="button" class="btn btn-success float-left ml-1">Create accounts</button>
        </div>
    </ul>


</div>

<section class="no-padding-top no-padding-bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="statistic-block block">
                    <div class="progress-details d-flex align-items-end justify-content-between">
                        <div class="title">
                            <div class="icon"><i class="icon-user-1"></i></div><strong>Accounts</strong>
                        </div>
                        <div class="number dashtext-1">{{$count}}</div>
                    </div>
                    <div class="progress progress-template">
                        <div role="progressbar" style="width: {{$count}}%" aria-valuenow="{{$count}}" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-1"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="statistic-block block">
                    <div class="progress-details d-flex align-items-end justify-content-between">
                        <div class="title">
                            <div class="icon"><i class="icon-contract"></i></div><strong>Max number of bots</strong>
                        </div>
                        <div class="number dashtext-2">10</div>
                    </div>
                    <div class="progress progress-template">
                        <div role="progressbar" style="width: 100%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-2"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="statistic-block block">
                    <div class="progress-details d-flex align-items-end justify-content-between">
                        <div class="title">
                            <div class="icon"><i class="icon-paper-and-pencil"></i></div><strong>Bots running</strong>
                        </div>
                        <div class="number dashtext-3">0</div>
                    </div>
                    <div class="progress progress-template">
                        <div role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg">
                <div class="block">
                    <div class="title"><strong>Accounts</strong></div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Summoner name</th>
                                <th>Region</th>
                                <th>Level(%)</th>
                                <th>Status</th>
                                <th>Bot ID</th>
                                <th>Total game time</th>
                                <th>BE</th>
                                <th>Information</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($summoner as $s)
                            <tr>
                                <th scope="row"><input  type="checkbox" name="{{$s->id}}"></th>
                                <td>{{$s->name}}</td>
                                <td>{{strtoupper($s->region)}}</td>
                                <td>{{ App\Repositories\AccountsRepository::getSummonerLvl($s->name,$s->region) }} ({{ round(App\Repositories\AccountsRepository::getSummonerLvl($s->name,$s->region) / 0.3,0) }}%)</td>
                                <td>{{$s->status}}</td>
                                <td>{{$s->bot_id}}</td>
                                <td>{{$s->total_game_time}}</td>
                                <td>400</td>
                                <td><button type="button" class="btn btn-primary">Check info</button><button formaction="/delete/{{$s->id}}" type="button" onclick="delete_acc{{$s->id}}()" class="btn btn-danger ml-2">Remove</button></td>
                            </tr>
                                <script>
                                    function delete_acc{{$s->id}}() {
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: "You won't be able to revert this!",
                                            icon: 'warning',
                                            showCancelButton: false,
                                            showConfirmButton: false,

                                            html: '<p>Be sure you are exported this account</p><form><button class="btn btn-danger mt-5" formaction="/delete/{{$s->id}}" type="submit">Remove</button><form>',
                                        }).then((result) => {
                                            if (result.value) {

                                            }
                                        })
                                    }
                                </script>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<footer class="footer">
    <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
            <!-- Please do not remove the backlink to us unless you support us at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            <p class="no-margin-bottom">2019 &copy; Your company. Design by <a href="https://bootstrapious.com/p/bootstrap-4-dark-admin">Bootstrapious</a>.</p>
        </div>
    </div>
</footer>

<!-- Modal create accounts-->
<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><strong id="exampleModalLabel" class="modal-title">Add accounts</strong>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form action="/add" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Accounts</label>
                    <textarea class="form-control" placeholder="IMPORTANT! Be sure your accounts list looks like 'REGION:USERNAME:PASSWORD'" name="accounts" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                <button type="submit" onclick="success()"   class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--ENDMODAL CREATE ACCOUNTS-->
<script>
    function success(){
        $('#myModal').modal('hide');
        Swal.fire(
            'Well done',
            'Accounts added to list!',
            'success'
        )

    }
</script>

@endsection
